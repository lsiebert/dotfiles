#!/bin/bash

for f in "$1/"*.json; do
  output=$(json_verify < "$f" 2> /dev/null )
  echo "$f" >&2
  if [ "$output" != "JSON is valid" ]; then 
    echo "$f is invalid"
  fi
done
  
