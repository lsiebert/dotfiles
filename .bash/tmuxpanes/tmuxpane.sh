#!/bin/bash

tmux set-buffer "$*"
originalPane=`tmux display-message -p '#P'`
for currentPane in `tmux list-panes -F '#P'`; do
	if [[ $originalPane -eq $currentPane ]]; then
		continue
	fi	 
	tmux select-pane -t "$currentPane"
	tmux send  "C-z"
	tmux send "$*; fg 2>/dev/null" ENTER
 done
 tmux select-pane -t "$originalPane"
 tmux send  "$*" ENTER


