#!/bin/sh

git config --global user.name "Lawrence Siebert"
git config --global user.email "lawrencesiebert@gmail.com"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#git config --global init.templatedir '~/.git_template'
cp -rvf $DIR/.git_template/* ~/.git_template
