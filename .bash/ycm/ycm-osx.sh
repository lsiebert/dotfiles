#!/bin/bash

#assumes homebrew is installed

brew install cmake || brew upgrade cmake
brew install node  || brew upgrade node
brew install go || brew upgrade go
npm install -g typescript
cd ~/.vim/bundle/YouCompleteMe
git pull && git submodule update --init --recursive && ./install.py --clang-completer --gocode-completer --tern-completer  

