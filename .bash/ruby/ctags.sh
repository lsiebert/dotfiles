#!/bin/bash



#tags for rbenv
brew upgrade rbenv
brew install
mkdir -p ~/.rbenv/plugins
git clone git://github.com/tpope/rbenv-ctags.git \
  ~/.rbenv/plugins/rbenv-ctags
rbenv ctags

#tags for gems
gem install gem-ctags
gem ctags
gem install gem-ripper-tags


