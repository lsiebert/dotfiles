#!/bin/bash


brew install rbenv || brew upgrade rbenv
brew install rbenv-gem-rehash
brew install ruby-build
eval "$(rbenv init -)"

#rbenv may need to be added to path if the last command fails


