#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

# grab $PATH
path = os.environ['PATH'].split(':')

# remove 'PATH=' prefix from first var
if 'PATH=' == path[0][:5]:
    path[0] = path[0][5:] 

# normalize all paths
path = map(os.path.normpath, path)

# remove duplicates via a dictionary
clean = dict.fromkeys(path)

# combine back into one path
clean_path = ':'.join(clean.keys())

# dump to stdout
print(f"PATH={clean_path}")
