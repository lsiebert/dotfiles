if [[ $(which brew) == "/usr/local/bin/brew" ]]; then
  export BREW_PREFIX_SAVE="/usr/local"
else
#shellcheck disable=SC2155
 export BREW_PREFIX_SAVE=$(brew --prefix)
fi
#set up so that mac os x handles tmux-256colors correctly per https://gpanders.com/blog/the-definitive-guide-to-using-tmux-256color-on-macos/
export TERMINFO_DIRS=$TERMINFO_DIRS:$HOME/.local/share/terminfo
#add gnu grep to path
export PATH="/usr/local/opt/grep/libexec/gnubin:$PATH"
#lunavim path
export PATH=/Users/lsiebert/.local/bin:$PATH
export DATABASE_PASSWORD=''
export LSCOLORS=GxFxCxDxBxegedabagaced
export EDITOR=nvim

#shellcheck disable=SC1117
export PS1="\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\]\$ "
export CLICOLOR=1
export HOMEBREW_EDITOR='/usr/local/bin/nvim'

#golang stuff
export GOPATH="${HOME}/go"
if [[ -d "/usr/local/opt/go" ]]; then
  GO_BREWPATH=/usr/local/opt/go
else
  GO_BREWPATH=$(brew --prefix golang)
fi
export GOROOT="${GO_BREWPATH}/libexec"
export PATH="$PATH:${GOPATH}/bin:${GOROOT}/bin"


#phpbrew (php version manager)
# https://github.com/phpbrew/phpbrew
#[[ -e ~/.phpbrew/bashrc ]] && source ~/.phpbrew/bashrc

#shellcheck disable=SC1090
test -e "${HOME}/.iterm2_shell_integration.bash" && source "${HOME}/.iterm2_shell_integration.bash"
# #todo.txt integration
alias t='todo.sh -d ~/.todo.cfg'
export TODOTXT_SORT_COMMAND='env LC_COLLATE=C sort -k 2,2 -k 1,1n'

export PATH="$HOME/.local/share/lunarvim/lvim/utils/bin:/usr/local/sbin:$PATH"
alias editvim='$EDITOR ~/.vim/vimrc'
alias editbash='$EDITOR ~/.bashrc && . ~/.bashrc'
alias screenrez='system_profiler SPDisplaysDataType |grep Resolution'
alias ls='ls -Gh'

#typo
alias gti="git"

#shellcheck disable=SC1090
source ~/dotfiles/bash/.cd
#shellcheck disable=SC1090
source ~/dotfiles/bash/.bashalias

#ctags
alias ctr='ctags -R --languages=ruby --exclude=db --exclude=log .'
alias rtg='ripper-tags -R --exclude=vendor --exclude=db --exclude=log .'
alias ctj='ctags -R --languages=javascript --exclude=db --exclude=log ./app/assets/javascripts'

# function sync()
# {
# todo can pull without checkout, do that
  # ( ( branch_name="$(git symbolic-ref --short HEAD)"
  # git checkout -q master
  # git pull -q
  # git checkout -q "$branch_name"
  # ) & )
# }
#

#edit last migration
function elm()
{
  if [[ -d "db/migrate" ]]; then
  "$EDITOR" "$(find db/migrate/*.rb | tail -n 1)"
  else
    echo "Not in a rails directory"
  fi
}
   

function bsp ()
{
  brew search "$@" | sed -n '/==/!p' | xargs -P 32 brew info 
}

PATH_OF_GENERIC=~/workspace/tools/generic-c--sh

#generates a basic c makefile from a generic version
make_c() {
    "${PATH_OF_GENERIC}/./generic.sh" "$1"
    if [[ -z "$1" ]]; then
      echo "usage make_c [foldername]"
    else
      cd "$1" || exit
    fi
}


# find differences within a file giving start and end lines for both sections
function diff_sections {
  #shellcheck disable=SC2155
  local fname=$(basename "$1");
  #shellcheck disable=SC2155
  local tempfile=$(mktemp -t "$fname");
  #shellcheck disable=SC2155
  local tempfile2=$(mktemp -t "$fname");
  head -n "$3" "$1" | tail -n +"$2" > "$tempfile"
  head -n "$5" "$1" | tail -n +"$4" > "$tempfile2"
  nvimdiff "$tempfile" "$tempfile2" ;
  rm "$tempfile";
  rm "$tempfile2";
}


function perp {
  if [[ "$#" -lt 1 ]]; then
    echo "runs pipenv run python manage.py followed by any parameters"
  else
    pipenv run python manage.py "$@"
  fi
}
#get relative path from absolute pat
relpath(){ python -c "import os.path; print os.path.relpath('$1','${2:-$PWD}')" ; }
relativetohome(){ relpath "$PWD" "$HOME"; }


# pipenv
#store pipenv virtual envs inside project
#as opposed to global hash that breaks if you move directories
export PIPENV_VENV_IN_PROJECT=1

#HOMEBREW Settings
unset HOMEBREW_BUILD_FROM_SOURCE
export HOMEBREW_NO_AUTO_UPDATE=1
export HOMEBREW_NO_EMOJI=1
export HOMEBREW_NO_ANALYTICS=1

#golang settings


#z is a function that collects information about which directories and then populates a list for easy cding
#shellcheck disable=SC1090
. ~/.bash/z/z.sh


#bash completion
#shellcheck disable=SC1090
if [ -f "$BREW_PREFIX_SAVE/etc/bash_completion" ]; then
#shellcheck disable=SC1091
  . "$BREW_PREFIX_SAVE/etc/bash_completion"
fi

#nvm
# export NVM_DIR="/Users/lsiebert/.nvm"
# #shellcheck disable=SC1090
if [ -s "$NVM_DIR/nvm.sh" ]; then 
#shellcheck disable=SC1091
  . "$NVM_DIR/nvm.sh"  # This loads nvm
fi


#rbenv
eval "$(rbenv init -)"

#pyenv virtualenv setup
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
#set z as cd
eval "$(zoxide init bash)"
#init fzf
#shellcheck disable=SC1090
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
#shellcheck disable=SC1090
eval "$(python "$HOME/dotfiles/clean-path.py")"
