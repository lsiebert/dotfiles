setlocal tabstop=4
setlocal shiftwidth=4
setlocal expandtab
setlocal smarttab
setlocal softtabstop=4
setlocal foldmethod=indent
setlocal autoindent
setlocal foldnestmax=2
"set foldcolumn=2
setlocal smartindent

"mapping for yapf python formatter

if executable("yapf") 
  nnoremap <leader>f :0,$!yapf<cr>
endif
