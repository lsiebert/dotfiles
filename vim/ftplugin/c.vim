
fun! <SID>StripTrailingWhitespaces()
		let l = line(".")
		let c = col(".")
		%s/\s\+$//e
		call cursor(l, c)
endfun

set noexpandtab

autocmd BufWritePre * :call <SID>StripTrailingWhitespaces()
