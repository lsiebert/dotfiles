#taken in part from http://samuelmullen.com/articles/configuring_new_rails_projects_with_railsrc_and_templates/
gem 'factory_bot_rails'
gem 'react_on_rails', '~> 10'
gem 'dotenv'
gem 'bcrypt'
gem 'redis', '~> 3.0'

# Use ActiveModel has_secure_password
gem_group :development, :test do
 gem 'foreman'
 gem 'awesome_print'
end
gem_group :development do
  gem 'annotate'
  #gem 'guard'
  #gem 'guard-minitest'
  gem 'parser', '>=2.5'
  gem 'rubocop'
end

def git_add(string)
  "git add . && git commit -m '#{string}' && printf \"\n#{string}\n\" && "
end


dir_name = File.basename(Dir.getwd)
puts "begin install #{dir_name}"
run "#{git_add('initial commit')}" \
  'bundle install && ' \
  "#{git_add('bundle install ran')}" \
  'rails generate annotate:install && ' \
   "#{git_add('installed annotate')}" \
   'rails g react_on_rails:install --redux && ' \
   "#{git_add('installed react_on_rails with redux')}" \
  'rails g model user email:string:uniq password:digest && ' \
  'rails generate controller welcome --no-assets && ' \
  "#{git_add('generated user model and welcome controller')}" \
  'yarn add redux-saga axios && yarn && ' \
  "#{git_add('installed redux-saga & axio')}" \
  'bundle install && ' \
  'bundle exec spring binstub --all &&' \
  "#{git_add('bundle install ran')}" \
  "echo '#{dir_name} with postgres and react complete'"

#generate "minitest:install"

#guardfile = <<-EOL
  #guard :minitest, :all_on_start => false do
    #watch(%r{^test/(.*)_test\.rb$})
    #watch(%r{^lib/(.+)\.rb$})         { |m| "test/lib/\#{m[1]}_test.rb" }
    #watch(%r{^test/test_helper\.rb$}) { 'test' }

    #watch(%r{^app/(models|mailers|helpers)/(.+)\.rb$}) { |m|
      #"test/\#{m[1]}/\#{m[2]}_test.rb"
    #}
    #watch(%r{^app/controllers/api/(.+)_controller\.rb$}) { |m| "test/requests/\#{m[1]}_test.rb" }
  #end
#EOL

#create_file "Guardfile", guardfile

